<?php
/**
 * @file
 *   Installation, uninstallation, and requirements for Polymer API.
 */

/**
 * Implements hook_requirements().
 */
function polymer_requirements($phase) {
  $requirements = array();
  if ($phase == 'runtime') {
    // Fetch library info.
    $library = libraries_detect('polymer');

    // Set up Requirements array.
    $requirements['polymer'] = array(
      'title' => t('Polymer API'),
      'value' => (isset($library['version'])) ? $library['version'] : '',
      'severity' => REQUIREMENT_OK,
    );

    // If Libraries API is enabled but platform.js is not found within the
    // sites/all/libraries/polymer folder, then report an error.
    if ($library['installed'] === FALSE) {
      $requirements['polymer']['value'] = t('Polymer is not correctly installed');
      $requirements['polymer']['severity'] = REQUIREMENT_ERROR;
      $requirements['polymer']['description'] = t('Please install !polymer in <strong>@libraries</strong>', array(
        '!polymer' => l('Polymer', $library['download url']),
        '@libraries' => 'sites/all/libraries/polymer',
      ));
    }
  }

  return $requirements;
}

/**
 * Implements hook_uninstall().
 */
function polymer_uninstall() {
  variable_del('polymer_library_type');
  variable_del('polymer_always');
}
