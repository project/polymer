<?php

/**
 * @file
 *   Drush integration for Polymer.
 */

/**
 * Implements hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * For a list of recognized keys,
 * @see drush_parse_command()
 *
 * @return
 *   An associative array describing your command(s).
 */
function polymer_drush_command() {
  $items = array();

  $items['polymer-download-platform'] = array(
    'callback'    => '_polymer_drush_download_platform',
    'description' => dt('Downloads the Polymer Platform'),
    'aliases'     => array('pdlp'),
    'arguments'   => array(
      'path' => dt('Optional, not recommended. A destination for the Polymer Platform.'),
    ),
  );
  $items['polymer-download'] = array(
    'callback'    => '_polymer_drush_download',
    'description' => dt('Downloads an individual Polymer component'),
    'aliases'     => array('pdlc'),
    'arguments'   => array(
      'component' => dt('Required. The name of your Drupal module which supplies the web component. Example: polymer_flipcard.'),
      'path' => dt('Optional, not recommended. A destination for the Platform library. If omitted Drush will use the default location. NOTE: if you set this manually, it might mean that the Polymer Platform has to be manually invoked in your theme_preprocess function.'),
    ),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * `drush help <name-of-your-command>`
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function polymer_drush_help($section) {
  switch ($section) {
    case 'drush:polymer-download':
      return dt('Downloads Polymer Platform. Accepts an optional destination argument.');
    case 'drush:polymer-download-component':
      return dt('Downloads an individual Polymer component library. Accepts an optional destination argument.');
  }
}

/**
 * Helper function to download the Polymer Platform.
 */
function _polymer_drush_download_platform() {
  _polymer_drush_download('polymer');
}

/**
 * Helper function to download any Polymer library.
 */
function _polymer_drush_download() {
  $args = func_get_args();
  $dir = '';
  $drush_context = '';

  // If user did not supply machine name of module, abort.
  if (!isset($args[0])) {
    drush_log(dt('Please supply the Drupal module name of the component you wish to download. Example: polymer_flipcard.'), 'error');
    return;
  }
  else {
    // Look up Libraries API info for this component.
    if (module_exists('libraries') && function_exists('libraries_detect')) {
      $library = libraries_detect($args[0]);
    }
    else {
      drush_log(dt('Libraries API seems to have a problem. Could not download the library you requested.'), 'error');
      return;
    }
  }

  // If the user supplied a destination, use that path
  if ($args[1]) {
    $dir = $args[1];
  }
  // Otherwise we need to use sites/all/libraries/$arg[0]
  else {
    $drush_context = drush_get_context('DRUSH_DRUPAL_ROOT');
  }

  // Build full path.
  $library_path = 'sites/all/libraries/' . $library['module'];
  $path = $drush_context . '/' . $library_path;

  // Download locations.
  $filename_ext = basename($library['download url']);
  $filename = basename($library['download url'], '.zip');

  // If the directory exists and contains a recognized file, we're finished.
  if ($library['installed'] === TRUE) {
    drush_log(dt('@name already present. No download required.', array(
      '@name' => $library['module'],
    )), 'notice');
    return;
  }
  else {
    // Create the path if it does not exist.
    if (!is_dir($path)) {
      drush_op('mkdir', $path);
      drush_log(dt('Directory @path was created', array(
        '@path' => $library_path,
      )), 'notice');
    }

    // Remove any existing Polymer Platform directory
    if (is_dir($path)) {
      drush_log(dt('The existing @name was overwritten at @path', array(
        '@name' => $library['module'],
        '@path' => $library_path,
      )), 'notice');
    }
    // Remove any existing Polymer Platform zip archive
    if (is_file($path . '/' . $filename)) {
      drush_op('unlink', $filename);
    }

    // Set the directory to the download location.
    $olddir = getcwd();
    chdir($path);

    // Download the zip archive
    if (!drush_shell_exec('wget ' . $library['download url'])) {
      drush_shell_exec('curl -O ' . $library['download url']);
    }

    // If archive was downloaded, unzip it and delete.
    if (is_file($path . '/' . $filename)) {
      // Decompress the zip archive
      drush_shell_exec('unzip -qq -o -j ' . $filename);
      // Remove the zip archive
      drush_op('unlink', $filename);
      // Log success.
      drush_log(dt('The latest @name library has been downloaded to @path', array(
        '@name' => $library['module'],
        '@path' => $library_path,
      )), 'success');
    }
    else {
      // Log the error.
      drush_log(dt('Drush was unable to download @name to @path', array(
        '@name' => $library['module'],
        '@path' => $library_path,
      )), 'error');
    }
  }
}

/**
 * Implements drush_MODULE_post_COMMAND().
 */
function drush_polymer_post_pm_enable() {
  $extensions = func_get_args();

  // Deal with comma delimited extension list.
  if (strpos($extensions[0], ',') !== FALSE) {
    $extensions = explode(',', $extensions[0]);
  }

  // Download the Polymer Platform library.
  if (in_array('polymer', $extensions) && !drush_get_option('skip')) {
    _polymer_drush_download_platform();
  }
}
