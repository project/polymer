<?php
/**
 * @file
 *   Main module file for Polymer API.
 */

// Define some constants for fetching and placing the library during setup.
define('POLYMER_PLATFORM_DOWNLOAD_URI', 'https://github.com/Polymer/polymer/archive/v1.2.3.zip');

// Define a constant to keep track of library version.
define('POLYMER_PLATFORM_VERSION_REGEX', '@"version":\s+"([0-9\.-]+)"@');

/**
 * Implements hook_menu().
 */
function polymer_menu() {
  $items = array();

  // Admin menu item.
  $items['admin/config/development/polymer'] = array(
    'title' => 'Polymer API',
    'description' => 'Manage settings for Polymer API',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('polymer_admin'),
    'file' => 'includes/polymer.admin.inc',
    'access arguments' => array('administer polymer'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function polymer_permission() {
  return array(
    'administer polymer' => array(
      'title' => t('Administer Polymer API'),
      'description' => t('Perform administration tasks for Polymer.'),
    ),
  );
}

/**
 * Implements hook_libraries_info().
 *
 * @return array
 */
function polymer_libraries_info() {
  $libraries = array();

  // Define Polymer within Libraries API
  $libraries['polymer'] = array(
    'name' => t('Polymer'),
    'vendor url' => 'http://polymer-project.org',
    'download url' => POLYMER_PLATFORM_DOWNLOAD_URI,
    'version arguments' => array(
      'file' => 'bower.json',
      'pattern' => POLYMER_PLATFORM_VERSION_REGEX,
    ),
  );

  return $libraries;
}

/**
 * Returns the full path of Polymer Platform, along with the filename.
 *
 * @return string
 */
function _polymer_get_path() {
  $path = &drupal_static(__FUNCTION__);

  if ($path === NULL) {
    // Check for directory specified in hook_libraries_info().
    if (module_exists('libraries')) {
      $library_path = libraries_get_path('polymer');
      if (file_exists($library_path)) {
        $path = $library_path;
      }
    }
  }

  return $path;
}

/**
 * Implements hook_web_component_info().
 */
function polymer_web_component_info() {
  $components = array();
  $polymer_path = _polymer_get_path();
  $polymer_library_type = variable_get('polymer_library_type', 'full');

  switch ($polymer_library_type) {
    case 'full':
      $type = '';
      break;
    case 'mini':
      $type = '-mini';
      break;
    case 'micro':
      $type = '-micro';
      break;
  }

  $path = $polymer_path . '/polymer' . $type . '.html';

  $components['polymer'] = array(
    'name' => t('Polymer'),
    'description' => t('Core Polymer web component'),
    'path' => $path,
    'autoload' => variable_get('polymer_always', FALSE),
    'weight' => 0,
  );

  return $components;
}
