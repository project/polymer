<?php
/**
 * @file
 *   Admin include for Polymer API module.
 */

/**
 * Implements hook_admin().
 */
function polymer_admin() {
  $form = array();

  $form['polymer_always'] = array(
    '#type' => 'checkbox',
    '#title' => t('Always load the Polymer Platform?'),
    '#description' => t('By default, Platform only loads when attached to a web component field being themed for output. Enable this option to force Platform to load on every page.'),
    '#default_value' => variable_get('polymer_always', FALSE),
  );

  $form['polymer_library_type'] = array(
    '#type' => 'radios',
    '#title' => t('Desired feature set'),
    '#description' => t('For more information visit !polymer_experimental page.', array('!polymer_experimental' => l('Polymer - Experimental features & elements', 'https://www.polymer-project.org/1.0/docs/devguide/experimental.html#feature-layering'))),
    '#options' => array(
      'full' => t('Full (entire Polymer library)'),
      'mini' => t('Mini (enhancements for when Shadow DOM isn\'t supported)'),
      'micro' => t('Micro (bare-minimum for Custom Elements)'),
    ),
    '#default_value' => variable_get('polymer_library_type', 'full'),
  );

  return system_settings_form($form);
}
